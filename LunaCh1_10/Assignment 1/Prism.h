#pragma once
#include <Windows.h>
#include <vector>
#include <xnamath.h>
#include <Vertex.h>

class Prism
{
public:
    static const unsigned MinimumSlices;
    static const unsigned MaximumSlices;

private:
    std::vector<Vertex> m_Vertices;
    std::vector<UINT> m_Indices;
    float m_Height;
    unsigned m_Slices;
    XMFLOAT3 m_Position;
    float m_SliceRotation = 0.0f;
    float m_DeltaRotation = 30.0f;
    float m_TotalRotation = m_SliceRotation;

public:
    Prism(unsigned aSlices, float aHeight, FXMVECTOR aPosition);
    std::vector<Vertex>& getVertices();
    std::vector<UINT>& getIndices();
    unsigned getSliceCount() const;
    float getHeight() const;
    float getSliceRotation() const;
    XMVECTOR getPosition() const;
    static unsigned getMaxVertexCount();
    static unsigned getMaxIndexCount();
private:
    void Construct();
    void ConstructBase(float aYPosition, float aYNormal, CXMVECTOR aColor);
    void ConstructSides();
};