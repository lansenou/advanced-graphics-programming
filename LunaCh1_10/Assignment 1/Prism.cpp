#include "Prism.h"

const unsigned Prism::MinimumSlices = 3;
const unsigned Prism::MaximumSlices = 360;

Prism::Prism(unsigned aSlices, float aHeight, FXMVECTOR aPosition)
    : m_Height(aHeight),
    m_Slices(aSlices)
{
    XMStoreFloat3(&m_Position, aPosition);
    if(aSlices >= MinimumSlices && aSlices <= MaximumSlices)
    {
        Construct();
    }
    else
    {
        throw std::out_of_range("Vertices exceed maximum/minimum vertices");
    }
}

std::vector<Vertex>& Prism::getVertices()
{
    return m_Vertices;
}

std::vector<UINT>& Prism::getIndices()
{
    return m_Indices;
}

unsigned Prism::getSliceCount() const
{
    return m_Slices;
}

float Prism::getHeight() const
{
    return m_Height;
}

float Prism::getSliceRotation() const
{
    return m_SliceRotation;
}

XMVECTOR Prism::getPosition() const
{
    return XMLoadFloat3(&m_Position);
}

unsigned Prism::getMaxIndexCount()
{
    return MaximumSlices * 12;
}

unsigned Prism::getMaxVertexCount()
{
    return MaximumSlices * 2 + 2;
}

void Prism::Construct()
{
    ConstructBase(m_Height / 2, 1.0f, Colors::Blue);
    ConstructBase(-m_Height / 2, -1.0f, Colors::Red);
    ConstructSides();
}

// https://en.wikipedia.org/wiki/Prism_(geometry)
// http://www.teacherschoice.com.au/maths_library/area%20and%20sa/area_9.htm
void Prism::ConstructBase(float a_YPosition, float a_YNormal, CXMVECTOR a_Color)
{
    const auto BaseIndex = m_Vertices.size();
    const auto RotationDelta = XM_2PI / m_Slices;

    for(auto i = 0u; i < m_Slices; i++)
    {
        float x = cos(i * RotationDelta);
        float z = sin(i * RotationDelta);

        m_Vertices.emplace_back(XMVectorSet(x, a_YPosition, z, 0.0f) + XMLoadFloat3(&m_Position), a_Color);
    }

    XMVECTOR center = XMVectorSet(0.0f, a_YPosition, 0.0f, 0.0f) + XMLoadFloat3(&m_Position);
    m_Vertices.emplace_back(center, a_Color);
    const auto CenterIndex = m_Vertices.size() - 1;

    for(auto i = 0u; i < m_Slices; i++)
    {
        m_Indices.push_back(CenterIndex);
        if(a_YNormal < 0.0f)
        {
            m_Indices.push_back(BaseIndex + i);
            m_Indices.push_back((i + 1) % m_Slices + BaseIndex);
        }
        else
        {
            m_Indices.push_back((i + 1) % m_Slices + BaseIndex);
            m_Indices.push_back(BaseIndex + i);
        }
    }
}

void Prism::ConstructSides()
{
    const auto IndicesPerSlice = 3;
    const auto IndicesPerBase = m_Slices * IndicesPerSlice;
    for(auto i = 0u; i < m_Slices; i++)
    {
        const auto Offset = IndicesPerSlice * i;
        m_Indices.push_back(m_Indices[2 + IndicesPerBase + Offset]);
        m_Indices.push_back(m_Indices[1 + IndicesPerBase + Offset]);
        m_Indices.push_back(m_Indices[2 + Offset]);

        m_Indices.push_back(m_Indices[1 + Offset]);
        m_Indices.push_back(m_Indices[2 + IndicesPerBase + Offset]);
        m_Indices.push_back(m_Indices[2 + Offset]);
    }
}
